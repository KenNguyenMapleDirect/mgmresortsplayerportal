<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Flipbook extends Model
{
    protected $table = 'May2021_Test';
    public $timestamps = false;
    protected $primaryKey = 'CSHRV_Account';
    public $incrementing = false;
}

