<?php

namespace App\Http\Controllers\Voyager;

use App\Data;
use App\Flipbook;
use App\User;
use App\WeekenderFlipbook;
use App\MayCoreSM;
use App\MayCorePC;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use League\Flysystem\Util;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Controller;

class VoyagerController extends \TCG\Voyager\Http\Controllers\VoyagerController
{
    public function index()
    {
        if (Auth::user()->role_id === 1 || Auth::user()->role_id === 4)
            return Voyager::view('voyager::index');
        else {
            //Get User from user Id
            $user = User::where('id', Auth::user()->id)->first();
            //define data
            $data = new \stdClass();
            $data->first_name = $user->first_name;
            $data->last_name = $user->last_name;
            $data->CSHRV_Tier = '';
            $data->CSHRV_MTD_Points = 0;
            $data->CSHRV_Comp_Dollars = 0;
            $data->CSHRV_Player_ID = $user->CSHRV_Player_ID;
            $data->CSHRV_Account = '';
            $data->CSHRV_Mailer_Type = '';
            $data->Weekender_Mailer_Type = '';
            $data->updated_at = now();
            //Update user data and flipbook data
            $datas = Data::where('CSHRV_Player_ID', Auth::user()->CSHRV_Player_ID)->first();
            if ($datas) {
                $data->first_name = $datas->first_name;
                $data->last_name = $datas->last_name;
                $data->CSHRV_Tier = $datas->CSHRV_Tier;
                $data->CSHRV_MTD_Points = $datas->CSHRV_MTD_Points;
                $data->CSHRV_Points = $datas->CSHRV_Points;
                $data->CSHRV_Comp_Dollars = $datas->CSHRV_Comp_Dollars;
                $data->CSHRV_Points_Next_Tier = $datas->CSHRV_Points_Next_Tier;
                $data->CSHRV_Player_ID = $datas->CSHRV_Player_ID;
                $data->CSHRV_MI = $datas->CSHRV_MI;
                $data->CSHRV_Host_Name = $datas->CSHRV_Host_Name;
                $data->CSHRV_Host_ID = $datas->CSHRV_Host_ID;
            }
            //Get Flipbook SM data
            $flipbookSMData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->where('CSHRV_Mailer_Type', 'Core SM')->first();
            if ($flipbookSMData) {
                $data->CSHRV_Account = $flipbookSMData->CSHRV_Account;
                $data->Flipbook_FName = $flipbookSMData->CSHRV_FName;
                $data->Flipbook_LName = $flipbookSMData->CSHRV_LName;
                $data->Flipbook_Tier = $flipbookSMData->CSHRV_Tier;
                $data->Flipbook_Version = $flipbookSMData->CSHRV_Version;
                $data->Flipbook_FP = $flipbookSMData->CSHRV_FP;
                $data->Flipbook_Total_FP = $flipbookSMData->CSHRV_Total_FP;
                $data->Flipbook_Food = $flipbookSMData->CSHRV_Food;
                $data->Flipbook_SBFP = $flipbookSMData->CSHRV_SBFP;
                $data->Flipbook_GC = $flipbookSMData->CSHRV_GC;
                $data->Flipbook_MFP = $flipbookSMData->CSHRV_MFP;
                $data->Flipbook_SGC = $flipbookSMData->CSHRV_SGC;
                $data->Flipbook_NC = $flipbookSMData->CSHRV_NC;
                $data->Flipbook_Hotel = $flipbookSMData->CSHRV_Hotel;
                $data->Flipbook_Hotel_Date_01 = $flipbookSMData->CSHRV_Hotel_Date_01;
                $data->Flipbook_Hotel_DOW = $flipbookSMData->CSHRV_Hotel_DOW;
                $data->first_name = $flipbookSMData->CSHRV_FName;
                $data->last_name = $flipbookSMData->CSHRV_LName;
                $data->result1 = $flipbookSMData->CSHRV_Img_Page01 . ".jpg";
                $data->result2 = $flipbookSMData->CSHRV_Img_Page02 . ".jpg";
                $data->result3 = $flipbookSMData->CSHRV_Img_Page03 . ".jpg";
                $data->result4 = $flipbookSMData->CSHRV_Img_Page04 . ".jpg";
                $data->result5 = $flipbookSMData->CSHRV_Img_Page05 . ".jpg";
                $data->result6 = $flipbookSMData->CSHRV_Img_Page06 . ".jpg";
                $data->CSHRV_Mailer_Type = $flipbookSMData->CSHRV_Mailer_Type;
            }

            //Get Flipbook PC data

            $flipbookPCData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->where('CSHRV_Mailer_Type', 'Core PC')->first();
            if ($flipbookPCData) {
                $data->CSHRV_Account = $flipbookPCData->CSHRV_Account;
                $data->Flipbook_FName = $flipbookPCData->CSHRV_FName;
                $data->Flipbook_LName = $flipbookPCData->CSHRV_LName;
                $data->Flipbook_Tier = $flipbookPCData->CSHRV_Tier;
                $data->Flipbook_Version = $flipbookPCData->CSHRV_Version;
                $data->Flipbook_FP = $flipbookPCData->CSHRV_FP;
                $data->Flipbook_Total_FP = $flipbookPCData->CSHRV_Total_FP;
                $data->Flipbook_Food = $flipbookPCData->CSHRV_Food;
                $data->Flipbook_SBFP = $flipbookPCData->CSHRV_SBFP;
                $data->Flipbook_GC = $flipbookPCData->CSHRV_GC;
                $data->Flipbook_MFP = $flipbookPCData->CSHRV_MFP;
                $data->Flipbook_SGC = $flipbookPCData->CSHRV_SGC;
                $data->Flipbook_NC = $flipbookPCData->CSHRV_NC;
                $data->Flipbook_Hotel = $flipbookPCData->CSHRV_Hotel;
                $data->Flipbook_Hotel_Date_01 = $flipbookPCData->CSHRV_Hotel_Date_01;
                $data->Flipbook_Hotel_DOW = $flipbookPCData->CSHRV_Hotel_DOW;
                $data->first_name = $flipbookPCData->CSHRV_FName;
                $data->last_name = $flipbookPCData->CSHRV_LName;
                $data->result2 = $flipbookPCData->CSHRV_Img_Page01 . ".jpg";
                $data->result3 = $flipbookPCData->CSHRV_Img_Page02 . ".jpg";
                $data->CSHRV_Mailer_Type = $flipbookPCData->CSHRV_Mailer_Type;
            }

            //Get Weekender data

            $weekenderData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->where('CSHRV_Mailer_Type', 'Weekender PC')->first();
            if ($weekenderData) {
                $data->weekenderData_result2 = $weekenderData->CSHRV_Img_Page01 . ".jpg";
                $data->weekenderData_result3 = $weekenderData->CSHRV_Img_Page02 . ".jpg";
                $data->Weekender_Mailer_Type = $weekenderData->CSHRV_Mailer_Type;
            }

//            // Get May_Core_PC data if have and overwrite
//            $weekenderData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
//            if($weekenderData){
//                $data->Weekender_Flipbook_Account = $weekenderData->CSHRV_Account;
//                $data->Weekender_Flipbook_FName = $weekenderData->Flipbook_FName;
//                $data->Weekender_Flipbook_LName = $weekenderData->Flipbook_LName;
//                $data->Weekender_Flipbook_Tier = $weekenderData->Flipbook_Tier;
//                $data->Weekender_Flipbook_Version = $weekenderData->Flipbook_Version;
//                $data->Weekender_Flipbook_FP = $weekenderData->Flipbook_FP;
//                $data->Weekender_Flipbook_Total_FP = $weekenderData->Flipbook_Total_FP;
//                $data->Weekender_Flipbook_Food = $weekenderData->Flipbook_Food;
//                $data->Weekender_Flipbook_SBFP = $weekenderData->Flipbook_SBFP;
//                $data->Weekender_Flipbook_GC = $weekenderData->Flipbook_GC;
//                $data->Weekender_Flipbook_MFP = $weekenderData->Flipbook_MFP;
//                $data->Weekender_Flipbook_SGC = $weekenderData->Flipbook_SGC;
//                $data->Weekender_Flipbook_NC = $weekenderData->Flipbook_NC;
//                $data->Weekender_Flipbook_Hotel = $weekenderData->Flipbook_Hotel;
//                $data->Weekender_Flipbook_Hotel_Date_01 = $weekenderData->Flipbook_Hotel_Date_01;
//                $data->Weekender_Flipbook_Hotel_DOW = $weekenderData->Flipbook_Hotel_DOW;
//                $data->first_name = $weekenderData->Flipbook_FName;
//                $data->last_name = $weekenderData->Flipbook_LName;
//            }

            //get FlipbookData
            // $flipbookData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
            // if ($flipbookData) {
            // $data->CSHRV_Account = $flipbookData->CSHRV_Account;
            // $data->Flipbook_FName = $flipbookData->Flipbook_FName;
            // $data->Flipbook_LName = $flipbookData->Flipbook_LName;
            // $data->Flipbook_Tier = $flipbookData->Flipbook_Tier;
            // $data->Flipbook_Version = $flipbookData->Flipbook_Version;
            // $data->Flipbook_FP = $flipbookData->Flipbook_FP;
            // $data->Flipbook_Total_FP = $flipbookData->Flipbook_Total_FP;
            // $data->Flipbook_Food = $flipbookData->Flipbook_Food;
            // $data->Flipbook_SBFP = $flipbookData->Flipbook_SBFP;
            // $data->Flipbook_GC = $flipbookData->Flipbook_GC;
            // $data->Flipbook_MFP = $flipbookData->Flipbook_MFP;
            // $data->Flipbook_SGC = $flipbookData->Flipbook_SGC;
            // $data->Flipbook_NC = $flipbookData->Flipbook_NC;
            // $data->Flipbook_Hotel = $flipbookData->Flipbook_Hotel;
            // $data->Flipbook_Hotel_Date_01 = $flipbookData->Flipbook_Hotel_Date_01;
            // $data->Flipbook_Hotel_DOW = $flipbookData->Flipbook_Hotel_DOW;
            // $data->first_name = $flipbookData->Flipbook_FName;
            // $data->last_name = $flipbookData->Flipbook_LName;
            // }
            //get WeekenderData
            // $weekenderData = WeekenderFlipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
            // if ($weekenderData) {
            // $data->Weekender_Flipbook_Account = $weekenderData->CSHRV_Account;
            // $data->Weekender_Flipbook_FName = $weekenderData->Flipbook_FName;
            // $data->Weekender_Flipbook_LName = $weekenderData->Flipbook_LName;
            // $data->Weekender_Flipbook_Tier = $weekenderData->Flipbook_Tier;
            // $data->Weekender_Flipbook_Version = $weekenderData->Flipbook_Version;
            // $data->WeekenderFlipbook_Offer_01 = $weekenderData->Flipbook_Offer_01;
            // $data->Weekender_Flipbook_SGC = $weekenderData->Flipbook_SGC;
            // $data->Weekender_Flipbook_Level = $flipbookData->Flipbook_Level;
            // $data->Weekender_Flipbook_Total_FP = $flipbookData->Flipbook_Total_FP;
            // $data->Weekender_Flipbook_MFP = $flipbookData->Flipbook_MFP;
            // }
            return view('player-dashboard')->with('data', $data);
        }
    }

    public function viewFlipbookByAccountIDApril()
    {
        return view('flipbook-by-account-id-april');
    }

    public function viewFlipbookByAccountIDMay()
    {
        return view('flipbook-by-account-id-may');
    }

    public function viewPlayerDashBoardByAccountId(Request $request)
    {
        $validated = $request->validate([
            'account_id' => 'required|exists:Datas,CSHRV_Player_ID',
        ]);
        $accountId = $request->input('account_id');
        //Get User from Datas table
        $datas = Data::where('CSHRV_Player_ID', $accountId)->first();
        //get user data and flipbook data
        $data = new \stdClass();
        $data->CSHRV_Account = '';
        $data->Weekender_Flipbook_Account = '';
        $data->first_name = $datas->CSHRV_FName;
        $data->last_name = $datas->CSHRV_LName;
        $data->CSHRV_Tier = $datas->CSHRV_Tier;
        $data->CSHRV_MTD_Points = $datas->CSHRV_MTD_Points;
        $data->CSHRV_Points = $datas->CSHRV_Points;
        $data->CSHRV_Comp_Dollars = $datas->CSHRV_Comp_Dollars;
        $data->CSHRV_Points_Next_Tier = $datas->CSHRV_Points_Next_Tier;
        $data->CSHRV_Player_ID = $datas->CSHRV_Player_ID;
        $data->CSHRV_Player_ID = $datas->CSHRV_Player_ID;
        $data->CSHRV_MI = $datas->CSHRV_MI;
        $data->CSHRV_Host_Name = $datas->CSHRV_Host_Name;
        $data->CSHRV_Host_ID = $datas->CSHRV_Host_ID;
        $data->updated_at = now();
        $data->CSHRV_Mailer_Type = '';
        $data->Weekender_Mailer_Type = '';
        //Get Flipbook SM data
        $flipbookSMData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->where('CSHRV_Mailer_Type', 'Core SM')->first();
        if ($flipbookSMData) {
            $data->CSHRV_Account = $flipbookSMData->CSHRV_Account;
            $data->Flipbook_FName = $flipbookSMData->CSHRV_FName;
            $data->Flipbook_LName = $flipbookSMData->CSHRV_LName;
            $data->Flipbook_Tier = $flipbookSMData->CSHRV_Tier;
            $data->Flipbook_Version = $flipbookSMData->CSHRV_Version;
            $data->Flipbook_FP = $flipbookSMData->CSHRV_FP;
            $data->Flipbook_Total_FP = $flipbookSMData->CSHRV_Total_FP;
            $data->Flipbook_Food = $flipbookSMData->CSHRV_Food;
            $data->Flipbook_SBFP = $flipbookSMData->CSHRV_SBFP;
            $data->Flipbook_GC = $flipbookSMData->CSHRV_GC;
            $data->Flipbook_MFP = $flipbookSMData->CSHRV_MFP;
            $data->Flipbook_SGC = $flipbookSMData->CSHRV_SGC;
            $data->Flipbook_NC = $flipbookSMData->CSHRV_NC;
            $data->Flipbook_Hotel = $flipbookSMData->CSHRV_Hotel;
            $data->Flipbook_Hotel_Date_01 = $flipbookSMData->CSHRV_Hotel_Date_01;
            $data->Flipbook_Hotel_DOW = $flipbookSMData->CSHRV_Hotel_DOW;
            $data->first_name = $flipbookSMData->CSHRV_FName;
            $data->last_name = $flipbookSMData->CSHRV_LName;
            $data->result1 = $flipbookSMData->CSHRV_Img_Page01 . ".jpg";
            $data->result2 = $flipbookSMData->CSHRV_Img_Page02 . ".jpg";
            $data->result3 = $flipbookSMData->CSHRV_Img_Page03 . ".jpg";
            $data->result4 = $flipbookSMData->CSHRV_Img_Page04 . ".jpg";
            $data->result5 = $flipbookSMData->CSHRV_Img_Page05 . ".jpg";
            $data->result6 = $flipbookSMData->CSHRV_Img_Page06 . ".jpg";
            $data->CSHRV_Mailer_Type = $flipbookSMData->CSHRV_Mailer_Type;
        }

        //Get Flipbook PC data

        $flipbookPCData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->where('CSHRV_Mailer_Type', 'Core PC')->first();
        if ($flipbookPCData) {
            $data->CSHRV_Account = $flipbookPCData->CSHRV_Account;
            $data->Flipbook_FName = $flipbookPCData->CSHRV_FName;
            $data->Flipbook_LName = $flipbookPCData->CSHRV_LName;
            $data->Flipbook_Tier = $flipbookPCData->CSHRV_Tier;
            $data->Flipbook_Version = $flipbookPCData->CSHRV_Version;
            $data->Flipbook_FP = $flipbookPCData->CSHRV_FP;
            $data->Flipbook_Total_FP = $flipbookPCData->CSHRV_Total_FP;
            $data->Flipbook_Food = $flipbookPCData->CSHRV_Food;
            $data->Flipbook_SBFP = $flipbookPCData->CSHRV_SBFP;
            $data->Flipbook_GC = $flipbookPCData->CSHRV_GC;
            $data->Flipbook_MFP = $flipbookPCData->CSHRV_MFP;
            $data->Flipbook_SGC = $flipbookPCData->CSHRV_SGC;
            $data->Flipbook_NC = $flipbookPCData->CSHRV_NC;
            $data->Flipbook_Hotel = $flipbookPCData->CSHRV_Hotel;
            $data->Flipbook_Hotel_Date_01 = $flipbookPCData->CSHRV_Hotel_Date_01;
            $data->Flipbook_Hotel_DOW = $flipbookPCData->CSHRV_Hotel_DOW;
            $data->first_name = $flipbookPCData->CSHRV_FName;
            $data->last_name = $flipbookPCData->CSHRV_LName;
            $data->result2 = $flipbookPCData->CSHRV_Img_Page01 . ".jpg";
            $data->result3 = $flipbookPCData->CSHRV_Img_Page02 . ".jpg";
            $data->CSHRV_Mailer_Type = $flipbookPCData->CSHRV_Mailer_Type;
        }

        //Get Weekender data

        $weekenderData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->where('CSHRV_Mailer_Type', 'Weekender PC')->first();
        if ($weekenderData) {
            $data->weekenderData_result2 = $weekenderData->CSHRV_Img_Page01 . ".jpg";
            $data->weekenderData_result3 = $weekenderData->CSHRV_Img_Page02 . ".jpg";
            $data->Weekender_Mailer_Type = $weekenderData->CSHRV_Mailer_Type;
        }

//        //Get May_Core_SM data
//        $flipbookData = MayCoreSM::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
//        if($flipbookData){
//            $data->CSHRV_Account = $flipbookData->CSHRV_Account;
//            $data->Flipbook_FName = $flipbookData->Flipbook_FName;
//            $data->Flipbook_LName = $flipbookData->Flipbook_LName;
//            $data->Flipbook_Tier = $flipbookData->Flipbook_Tier;
//            $data->Flipbook_Version = $flipbookData->Flipbook_Version;
//            $data->Flipbook_FP = $flipbookData->Flipbook_FP;
//            $data->Flipbook_Total_FP = $flipbookData->Flipbook_Total_FP;
//            $data->Flipbook_Food = $flipbookData->Flipbook_Food;
//            $data->Flipbook_SBFP = $flipbookData->Flipbook_SBFP;
//            $data->Flipbook_GC = $flipbookData->Flipbook_GC;
//            $data->Flipbook_MFP = $flipbookData->Flipbook_MFP;
//            $data->Flipbook_SGC = $flipbookData->Flipbook_SGC;
//            $data->Flipbook_NC = $flipbookData->Flipbook_NC;
//            $data->Flipbook_Hotel = $flipbookData->Flipbook_Hotel;
//            $data->Flipbook_Hotel_Date_01 = $flipbookData->Flipbook_Hotel_Date_01;
//            $data->Flipbook_Hotel_DOW = $flipbookData->Flipbook_Hotel_DOW;
//            $data->first_name = $flipbookData->Flipbook_FName;
//            $data->last_name = $flipbookData->Flipbook_LName;
//        }
//        // Get May_Core_PC data if have and overwrite
//        $weekenderData = MayCorePC::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
//        if($weekenderData){
//            $data->Weekender_Flipbook_Account = $weekenderData->CSHRV_Account;
//            $data->Weekender_Flipbook_FName = $weekenderData->Flipbook_FName;
//            $data->Weekender_Flipbook_LName = $weekenderData->Flipbook_LName;
//            $data->Weekender_Flipbook_Tier = $weekenderData->Flipbook_Tier;
//            $data->Weekender_Flipbook_Version = $weekenderData->Flipbook_Version;
//            $data->Weekender_Flipbook_FP = $weekenderData->Flipbook_FP;
//            $data->Weekender_Flipbook_Total_FP = $weekenderData->Flipbook_Total_FP;
//            $data->Weekender_Flipbook_Food = $weekenderData->Flipbook_Food;
//            $data->Weekender_Flipbook_SBFP = $weekenderData->Flipbook_SBFP;
//            $data->Weekender_Flipbook_GC = $weekenderData->Flipbook_GC;
//            $data->Weekender_Flipbook_MFP = $weekenderData->Flipbook_MFP;
//            $data->Weekender_Flipbook_SGC = $weekenderData->Flipbook_SGC;
//            $data->Weekender_Flipbook_NC = $weekenderData->Flipbook_NC;
//            $data->Weekender_Flipbook_Hotel = $weekenderData->Flipbook_Hotel;
//            $data->Weekender_Flipbook_Hotel_Date_01 = $weekenderData->Flipbook_Hotel_Date_01;
//            $data->Weekender_Flipbook_Hotel_DOW = $weekenderData->Flipbook_Hotel_DOW;
//            $data->first_name = $weekenderData->Flipbook_FName;
//            $data->last_name = $weekenderData->Flipbook_LName;
//        }
//
//        $flipbookData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
//        if ($flipbookData) {
//            $data->CSHRV_Account = $flipbookData->CSHRV_Account;
//            $data->Flipbook_FName = $flipbookData->Flipbook_FName;
//            $data->Flipbook_LName = $flipbookData->Flipbook_LName;
//            $data->Flipbook_Tier = $flipbookData->Flipbook_Tier;
//            $data->Flipbook_Version = $flipbookData->Flipbook_Version;
//            $data->Flipbook_FP = $flipbookData->Flipbook_FP;
//            $data->Flipbook_Total_FP = $flipbookData->Flipbook_Total_FP;
//            $data->Flipbook_Food = $flipbookData->Flipbook_Food;
//            $data->Flipbook_SBFP = $flipbookData->Flipbook_SBFP;
//            $data->Flipbook_GC = $flipbookData->Flipbook_GC;
//            $data->Flipbook_MFP = $flipbookData->Flipbook_MFP;
//            $data->Flipbook_SGC = $flipbookData->Flipbook_SGC;
//            $data->Flipbook_NC = $flipbookData->Flipbook_NC;
//            $data->Flipbook_Hotel = $flipbookData->Flipbook_Hotel;
//            $data->Flipbook_Hotel_Date_01 = $flipbookData->Flipbook_Hotel_Date_01;
//            $data->Flipbook_Hotel_DOW = $flipbookData->Flipbook_Hotel_DOW;
//        }
//
//        //get WeekenderData
//        $weekenderData = WeekenderFlipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
//        if ($weekenderData) {
//            $data->Weekender_Flipbook_Account = $weekenderData->CSHRV_Account;
//            $data->Weekender_Flipbook_FName = $weekenderData->Flipbook_FName;
//            $data->Weekender_Flipbook_LName = $weekenderData->Flipbook_LName;
//            $data->Weekender_Flipbook_Tier = $weekenderData->Flipbook_Tier;
//            $data->Weekender_Flipbook_Version = $weekenderData->Flipbook_Version;
//            $data->WeekenderFlipbook_Offer_01 = $weekenderData->Flipbook_Offer_01;
//            $data->Weekender_Flipbook_SGC = $weekenderData->Flipbook_SGC;
//            $data->Weekender_Flipbook_Level = $flipbookData->Flipbook_Level;
//            $data->Weekender_Flipbook_Total_FP = $flipbookData->Flipbook_Total_FP;
//            $data->Weekender_Flipbook_MFP = $flipbookData->Flipbook_MFP;
//        }
        return view('player-dashboard-by-account-id')->with('data', $data);
    }

    public function getViewPlayerDashBoardByAccountId($accountId)
    {
        //Get User from user Id
        $datas = Data::where('CSHRV_Player_ID', $accountId)->first();
        //get user data and flipbook data
        $data = new \stdClass();
        $data->CSHRV_Account = '';
        $data->Weekender_Flipbook_Account = '';
        $data->first_name = $datas->CSHRV_FName;
        $data->last_name = $datas->CSHRV_LName;
        $data->CSHRV_Tier = $datas->CSHRV_Tier;
        $data->CSHRV_MTD_Points = $datas->CSHRV_MTD_Points;
        $data->CSHRV_Points = $datas->CSHRV_Points;
        $data->CSHRV_Comp_Dollars = $datas->CSHRV_Comp_Dollars;
        $data->CSHRV_Points_Next_Tier = $datas->CSHRV_Points_Next_Tier;
        $data->CSHRV_Player_ID = $datas->CSHRV_Player_ID;
        $data->CSHRV_Player_ID = $datas->CSHRV_Player_ID;
        $data->CSHRV_MI = $datas->CSHRV_MI;
        $data->CSHRV_Host_Name = $datas->CSHRV_Host_Name;
        $data->CSHRV_Host_ID = $datas->CSHRV_Host_ID;
        $data->updated_at = now();
        $data->CSHRV_Mailer_Type = '';
        $data->Weekender_Mailer_Type = '';
        //Get Flipbook SM data
        $flipbookSMData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->where('CSHRV_Mailer_Type', 'Core SM')->first();
        if ($flipbookSMData) {
            $data->CSHRV_Account = $flipbookSMData->CSHRV_Account;
            $data->Flipbook_FName = $flipbookSMData->CSHRV_FName;
            $data->Flipbook_LName = $flipbookSMData->CSHRV_LName;
            $data->Flipbook_Tier = $flipbookSMData->CSHRV_Tier;
            $data->Flipbook_Version = $flipbookSMData->CSHRV_Version;
            $data->Flipbook_FP = $flipbookSMData->CSHRV_FP;
            $data->Flipbook_Total_FP = $flipbookSMData->CSHRV_Total_FP;
            $data->Flipbook_Food = $flipbookSMData->CSHRV_Food;
            $data->Flipbook_SBFP = $flipbookSMData->CSHRV_SBFP;
            $data->Flipbook_GC = $flipbookSMData->CSHRV_GC;
            $data->Flipbook_MFP = $flipbookSMData->CSHRV_MFP;
            $data->Flipbook_SGC = $flipbookSMData->CSHRV_SGC;
            $data->Flipbook_NC = $flipbookSMData->CSHRV_NC;
            $data->Flipbook_Hotel = $flipbookSMData->CSHRV_Hotel;
            $data->Flipbook_Hotel_Date_01 = $flipbookSMData->CSHRV_Hotel_Date_01;
            $data->Flipbook_Hotel_DOW = $flipbookSMData->CSHRV_Hotel_DOW;
            $data->first_name = $flipbookSMData->CSHRV_FName;
            $data->last_name = $flipbookSMData->CSHRV_LName;
            $data->result1 = $flipbookSMData->CSHRV_Img_Page01 . ".jpg";
            $data->result2 = $flipbookSMData->CSHRV_Img_Page02 . ".jpg";
            $data->result3 = $flipbookSMData->CSHRV_Img_Page03 . ".jpg";
            $data->result4 = $flipbookSMData->CSHRV_Img_Page04 . ".jpg";
            $data->result5 = $flipbookSMData->CSHRV_Img_Page05 . ".jpg";
            $data->result6 = $flipbookSMData->CSHRV_Img_Page06 . ".jpg";
            $data->CSHRV_Mailer_Type = $flipbookSMData->CSHRV_Mailer_Type;
        }

        //Get Flipbook PC data

        $flipbookPCData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->where('CSHRV_Mailer_Type', 'Core PC')->first();
        if ($flipbookPCData) {
            $data->CSHRV_Account = $flipbookPCData->CSHRV_Account;
            $data->Flipbook_FName = $flipbookPCData->CSHRV_FName;
            $data->Flipbook_LName = $flipbookPCData->CSHRV_LName;
            $data->Flipbook_Tier = $flipbookPCData->CSHRV_Tier;
            $data->Flipbook_Version = $flipbookPCData->CSHRV_Version;
            $data->Flipbook_FP = $flipbookPCData->CSHRV_FP;
            $data->Flipbook_Total_FP = $flipbookPCData->CSHRV_Total_FP;
            $data->Flipbook_Food = $flipbookPCData->CSHRV_Food;
            $data->Flipbook_SBFP = $flipbookPCData->CSHRV_SBFP;
            $data->Flipbook_GC = $flipbookPCData->CSHRV_GC;
            $data->Flipbook_MFP = $flipbookPCData->CSHRV_MFP;
            $data->Flipbook_SGC = $flipbookPCData->CSHRV_SGC;
            $data->Flipbook_NC = $flipbookPCData->CSHRV_NC;
            $data->Flipbook_Hotel = $flipbookPCData->CSHRV_Hotel;
            $data->Flipbook_Hotel_Date_01 = $flipbookPCData->CSHRV_Hotel_Date_01;
            $data->Flipbook_Hotel_DOW = $flipbookPCData->CSHRV_Hotel_DOW;
            $data->first_name = $flipbookPCData->CSHRV_FName;
            $data->last_name = $flipbookPCData->CSHRV_LName;
            $data->result2 = $flipbookPCData->CSHRV_Img_Page01 . ".jpg";
            $data->result3 = $flipbookPCData->CSHRV_Img_Page02 . ".jpg";
            $data->CSHRV_Mailer_Type = $flipbookPCData->CSHRV_Mailer_Type;
        }

        //Get Weekender data

        $weekenderData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->where('CSHRV_Mailer_Type', 'Weekender PC')->first();
        if ($weekenderData) {
            $data->weekenderData_result2 = $weekenderData->CSHRV_Img_Page01 . ".jpg";
            $data->weekenderData_result3 = $weekenderData->CSHRV_Img_Page02 . ".jpg";
            $data->Weekender_Mailer_Type = $weekenderData->CSHRV_Mailer_Type;
        }

//        //Get May_Core_SM data
//        $flipbookData = MayCoreSM::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
//        if($flipbookData){
//            $data->CSHRV_Account = $flipbookData->CSHRV_Account;
//            $data->Flipbook_FName = $flipbookData->Flipbook_FName;
//            $data->Flipbook_LName = $flipbookData->Flipbook_LName;
//            $data->Flipbook_Tier = $flipbookData->Flipbook_Tier;
//            $data->Flipbook_Version = $flipbookData->Flipbook_Version;
//            $data->Flipbook_FP = $flipbookData->Flipbook_FP;
//            $data->Flipbook_Total_FP = $flipbookData->Flipbook_Total_FP;
//            $data->Flipbook_Food = $flipbookData->Flipbook_Food;
//            $data->Flipbook_SBFP = $flipbookData->Flipbook_SBFP;
//            $data->Flipbook_GC = $flipbookData->Flipbook_GC;
//            $data->Flipbook_MFP = $flipbookData->Flipbook_MFP;
//            $data->Flipbook_SGC = $flipbookData->Flipbook_SGC;
//            $data->Flipbook_NC = $flipbookData->Flipbook_NC;
//            $data->Flipbook_Hotel = $flipbookData->Flipbook_Hotel;
//            $data->Flipbook_Hotel_Date_01 = $flipbookData->Flipbook_Hotel_Date_01;
//            $data->Flipbook_Hotel_DOW = $flipbookData->Flipbook_Hotel_DOW;
//            $data->first_name = $flipbookData->Flipbook_FName;
//            $data->last_name = $flipbookData->Flipbook_LName;
//        }
//        // Get May_Core_PC data if have and overwrite
//        $weekenderData = MayCorePC::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
//        if($weekenderData){
//            $data->Weekender_Flipbook_Account = $weekenderData->CSHRV_Account;
//            $data->Weekender_Flipbook_FName = $weekenderData->Flipbook_FName;
//            $data->Weekender_Flipbook_LName = $weekenderData->Flipbook_LName;
//            $data->Weekender_Flipbook_Tier = $weekenderData->Flipbook_Tier;
//            $data->Weekender_Flipbook_Version = $weekenderData->Flipbook_Version;
//            $data->Weekender_Flipbook_FP = $weekenderData->Flipbook_FP;
//            $data->Weekender_Flipbook_Total_FP = $weekenderData->Flipbook_Total_FP;
//            $data->Weekender_Flipbook_Food = $weekenderData->Flipbook_Food;
//            $data->Weekender_Flipbook_SBFP = $weekenderData->Flipbook_SBFP;
//            $data->Weekender_Flipbook_GC = $weekenderData->Flipbook_GC;
//            $data->Weekender_Flipbook_MFP = $weekenderData->Flipbook_MFP;
//            $data->Weekender_Flipbook_SGC = $weekenderData->Flipbook_SGC;
//            $data->Weekender_Flipbook_NC = $weekenderData->Flipbook_NC;
//            $data->Weekender_Flipbook_Hotel = $weekenderData->Flipbook_Hotel;
//            $data->Weekender_Flipbook_Hotel_Date_01 = $weekenderData->Flipbook_Hotel_Date_01;
//            $data->Weekender_Flipbook_Hotel_DOW = $weekenderData->Flipbook_Hotel_DOW;
//            $data->first_name = $weekenderData->Flipbook_FName;
//            $data->last_name = $weekenderData->Flipbook_LName;
//        }

//        //Get Flipbook data
//        $flipbookData = Flipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
//        if ($flipbookData) {
//            $data->CSHRV_Account = $flipbookData->CSHRV_Account;
//            $data->Flipbook_FName = $flipbookData->Flipbook_FName;
//            $data->Flipbook_LName = $flipbookData->Flipbook_LName;
//            $data->Flipbook_Tier = $flipbookData->Flipbook_Tier;
//            $data->Flipbook_Version = $flipbookData->Flipbook_Version;
//            $data->Flipbook_FP = $flipbookData->Flipbook_FP;
//            $data->Flipbook_Total_FP = $flipbookData->Flipbook_Total_FP;
//            $data->Flipbook_Food = $flipbookData->Flipbook_Food;
//            $data->Flipbook_SBFP = $flipbookData->Flipbook_SBFP;
//            $data->Flipbook_GC = $flipbookData->Flipbook_GC;
//            $data->Flipbook_MFP = $flipbookData->Flipbook_MFP;
//            $data->Flipbook_SGC = $flipbookData->Flipbook_SGC;
//            $data->Flipbook_NC = $flipbookData->Flipbook_NC;
//            $data->Flipbook_Hotel = $flipbookData->Flipbook_Hotel;
//            $data->Flipbook_Hotel_Date_01 = $flipbookData->Flipbook_Hotel_Date_01;
//            $data->Flipbook_Hotel_DOW = $flipbookData->Flipbook_Hotel_DOW;
//        }
//
//        //get WeekenderData
//        $weekenderData = WeekenderFlipbook::where('CSHRV_Account', $data->CSHRV_Player_ID)->first();
//        if ($weekenderData) {
//            $data->Weekender_Flipbook_Account = $weekenderData->CSHRV_Account;
//            $data->Weekender_Flipbook_FName = $weekenderData->Flipbook_FName;
//            $data->Weekender_Flipbook_LName = $weekenderData->Flipbook_LName;
//            $data->Weekender_Flipbook_Tier = $weekenderData->Flipbook_Tier;
//            $data->Weekender_Flipbook_Version = $weekenderData->Flipbook_Version;
//            $data->WeekenderFlipbook_Offer_01 = $weekenderData->Flipbook_Offer_01;
//            $data->Weekender_Flipbook_SGC = $weekenderData->Flipbook_SGC;
//            $data->Weekender_Flipbook_Level = $flipbookData->Flipbook_Level;
//            $data->Weekender_Flipbook_Total_FP = $flipbookData->Flipbook_Total_FP;
//            $data->Weekender_Flipbook_MFP = $flipbookData->Flipbook_MFP;
//        }
        return view('get-player-dashboard-by-account-id')->with('data', $data);
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('voyager.login');
    }

    public function upload(Request $request)
    {
        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = $request->input('type_slug');
        $file = $request->file('image');

        $path = $slug . '/' . date('F') . date('Y') . '/';

        $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk(config('voyager.storage.disk'))->exists($path . $filename . '.' . $file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()) . (string)($filename_counter++);
        }

        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
                ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            if ($ext !== 'gif') {
                $image->orientate();
            }
            $image->encode($file->getClientOriginalExtension(), 75);

            // move uploaded file from temp to uploads directory
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string)$image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }

        // echo out script that TinyMCE can handle and update the image in the editor
        return "<script> parent.helpers.setImageValue('" . Voyager::image($fullFilename) . "'); </script>";
    }
}
