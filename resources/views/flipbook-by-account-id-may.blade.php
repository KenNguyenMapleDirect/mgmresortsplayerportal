@extends('layouts.app')

@section('content')
    <div id="container">
        <div class="top-menu">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="/wowbook_assets/Logo.png" alt="" style="height: 40px;margin:5px">
                        </div>
                        <div class="col-md-6">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="font-size: 18px;color: #84724d;" aria-expanded="false">Monthly Offers
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu" id="MonthlyOffers">
                                        <li><a href="/view-flipbook-by-account-id-april" onclick="event.preventDefault();
                                             document.getElementById('go-to-april-form').submit();" style="font-size: 18px;">April 2021 Newsletter</a></li>
                                        <li><a href="#"  style="font-size: 18px;"><strong>May 2021 Newsletter</strong></a></li>
                                        <form id="go-to-april-form" action="{{ route('view-flipbook-by-account-id-april') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <a href="/" style="float:right;padding-top: 15px;padding-bottom: 15px;color: #84724d;padding-right: 4%;font-size:18px">Logout</a></li>
                    </div>
                </div>
            </div>
        </div>
        <nav>
            <ul>
                <li><a id='first' href="#" title='goto first page'>First page</a></li>
                <li><a id='back' href="#" title='go back one page'>Back</a></li>
                <li><a id='next' href="#" title='go foward one page'>Next</a></li>
                <li><a id='last' href="#" title='goto last page'>last page</a></li>
                <li><a id='zoomin' href="#" title='zoom in'>Zoom In</a></li>
                <li><a id='zoomout' href="#" title='zoom out'>Zoom Out</a></li>
                <li><a id='slideshow' href="#" title='start slideshow'>Slide Show</a></li>
                <li><a id='flipsound' href="#" title='flip sound on/off'>Flip sound</a></li>
                <li><a id='fullscreen' href="#" title='fullscreen on/off'>Fullscreen</a></li>
                <li><a id='thumbs' href="#" title='thumbnails on/off'>Thumbs</a></li>
            </ul>
        </nav>
        <div id="main">
            <div id='features'>
                <div class='responsive feature'>
                    <img src="/wowbook_assets/May_2021_pages/Page01.jpg" alt="" style="height: 100%;width: 100%">
                </div>
                <div class='responsive feature'>
                    <img src="/wowbook_assets/May_2021_pages/Page02.jpg" alt="" style="height: 100%;width: 100%">
                </div>
                <div class='responsive feature'>
                    <img src="/wowbook_assets/May_2021_pages/Page03.jpg" alt="" style="height: 100%;width: 100%">
                </div>
                <div class='responsive feature'>
                    <img src="/wowbook_assets/May_2021_pages/Page04.jpg" alt="" style="height: 100%;width: 100%">
                </div>
                <div class='responsive feature'>
                    <img src="/wowbook_assets/May_2021_pages/Page05.jpg" alt="" style="height: 100%;width: 100%">
                </div>
                <div class='responsive feature'>
                    <img src="/wowbook_assets/May_2021_pages/Page06.jpg" alt="" style="height: 100%;width: 100%">
                </div>
            </div> <!-- features -->

        </div>
        <div id='thumbs_holder'>
        </div>
        <footer>
        </footer>
    </div> <!--! end of #container -->
    <!-- Javascript at the bottom for fast page loading -->

    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>!window.jQuery && document.write(unescape('%3Cscript src="./js/libs/jquery-1.9.1.min.js"%3E%3C/script%3E'))</script>

    <script type="text/javascript" src="/wowbook_assets/wow_book.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#features').wowBook({
                height: 500
                , width: 800
                , centeredWhenClosed: true
                , hardcovers: true
                , turnPageDuration: 1000
                , numberedPages: [1, -2]
                , controls: {
                    zoomIn: '#zoomin',
                    zoomOut: '#zoomout',
                    next: '#next',
                    back: '#back',
                    first: '#first',
                    last: '#last',
                    slideShow: '#slideshow',
                    flipSound: '#flipsound',
                    thumbnails: '#thumbs',
                    fullscreen: '#fullscreen'
                }
                , scaleToFit: "#container"
                , thumbnailsPosition: 'bottom'
                , onFullscreenError: function () {
                    var msg = "Fullscreen failed.";
                    if (self != top) msg = "The frame is blocking full screen mode. Click on 'remove frame' button above and try to go full screen again."
                    alert(msg);
                }
            }).css({'display': 'none', 'margin': 'auto'}).fadeIn(1000);

            $("#cover").click(function () {
                $.wowBook("#features").advance();
            });

            var book = $.wowBook("#features");

            function rebuildThumbnails() {
                book.destroyThumbnails()
                book.showThumbnails()
                $("#thumbs_holder").css("marginTop", -$("#thumbs_holder").height() / 2)
            }

            $("#thumbs_position button").on("click", function () {
                var position = $(this).text().toLowerCase()
                if ($(this).data("customized")) {
                    position = "top"
                    book.opts.thumbnailsParent = "#thumbs_holder";
                } else {
                    book.opts.thumbnailsParent = "body";
                }
                book.opts.thumbnailsPosition = position
                rebuildThumbnails();
            })
            $("#thumb_automatic").click(function () {
                book.opts.thumbnailsSprite = null
                book.opts.thumbnailWidth = null
                rebuildThumbnails();
            })
            $("#thumb_sprite").click(function () {
                book.opts.thumbnailsSprite = "images/thumbs.jpg"
                book.opts.thumbnailWidth = 136
                rebuildThumbnails();
            })
            $("#thumbs_size button").click(function () {
                var factor = 0.02 * ($(this).index() ? -1 : 1);
                book.opts.thumbnailScale = book.opts.thumbnailScale + factor;
                rebuildThumbnails();
            })

        });
    </script>

    <!-- scripts concatenated and minified via ant build script-->
    <script src="/wowbook_assets/js/plugins.js"></script>
    <script src="/wowbook_assets/js/script.js"></script>
    <!-- end concatenated and minified scripts-->

    <!--[if lt IE 7 ]>
    <script src="/wowbook_assets/js/dd_belatedpng.js"></script>
    <script> DD_belatedPNG.fix('img, .png_bg'); //fix any <img> or .png_bg background-images </script>
    <![endif]-->
@endsection


