<!DOCTYPE html>
<html lang="en">
<head>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="icon"
          type="image/ico"
          href="/wowbook_assets/favicon.ico">

    <meta charset="utf-8">
    <title>MGM Grand Detroit</title>
    <style type="text/css">
        html, body {
            margin: 0;
            padding: 0;
            background-color: #84724d !important;
            font-family: 'Open Sans', sans-serif;
        }

        .wowbook-container {
            background-color: #162642 !important;
        }

        .login_table {
            margin: auto;
            margin-top: 181px;
            width: 600px;
            background-color: #fff;
            text-align: center;
            align-content: center;
            font-size: 32px;
            font-weight: 700;
        }

        .logo_dark {
            width: 300px;
            text-align: center;
            display: block;
            margin: 15px 0px 16px 0px;
        }

        .login_text_input {
            font-family: 'Open Sans', sans-serif;
            width: 542px;
            height: 48px;
            border: 1px solid #ccc;
            padding: 0px 7px;
            vertical-align: middle;
            text-align: left;
            color: #6f6f6f;
            margin-top: 32px;
            font-size: 12px;
        }

        .login_submit_input {
            font-family: 'Open Sans', sans-serif;
            width: 558px;
            height: 35px;
            border: 1px solid #ccc;
            padding: 0px;
            vertical-align: middle;
            text-align: center;
            color: #464646;
            margin-top: 32px;
            margin-bottom: 32px;
            font-size: 12px;
            cursor: pointer;
            background-color: #e7e7e7;
            border-color: #dadada;
        }

        .login_submit_input:hover {
            border-color: #bbb;
            background-color: #d3d3d3;
        }

        .login_error {
            margin-top: 30px;
            color: #f04124;
            font-size: 15px;
            font-family: 'Open Sans', sans-serif;
            font-weight: 400;
            line-height: 21px;
        }
    </style>
    <!-- CSS : implied media="all" -->
    <link rel="stylesheet" href="/wowbook_assets/css/style.css">
    <link rel="stylesheet" href="/wowbook_assets/css/wow_book.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/wowbook_assets/css/preview.css"><!-- Uncomment if you are specifically targeting less enabled mobile browsers
	<link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->

    <link href='//fonts.googleapis.com/css?family=News+Cycle' rel='stylesheet' type='text/css'>
    <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
    <script src="/wowbook_assets/js/modernizr-1.6.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="content">
    @yield('content')
</div>
</body>
</html>
