@extends('layouts.app')
@section('content')
<table class="login_table" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <img class="logo_dark" src="/wowbook_assets/Logo.png" style="display: block;">
            <b style="color: #84724d">Log In</b>
        </td>
    </tr>
    <tr>
        <td align="center">
            @if($errors->has('account_id') || $errors->has('account_id'))
                <span style="color: red">{{$errors->first('account_id') }}</span>
            @endif
            <form name="login" id="login" action="{{ route('view-flipbook-by-account-id-april') }}" method="post">
                {{ csrf_field() }}
                <input name="account_id" id="account_id" type="text" value="{{ old('account_id') }}"
                       class="login_text_input @if($errors->has('account_id')) has-error @endif" placeholder="Account #" required/>
                <input name="login" id="login" type="submit" value="Log In" class="login_submit_input"/></form>

        </td>
    </tr>
</table>
@endsection

