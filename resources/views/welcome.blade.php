<!doctype html>

<html class="no-js"  lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <!-- Force IE to use the latest rendering engine available -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!--Google Fonts Below-->
    <link rel="stylesheet" href="{{ asset('assets/css/phv1gon.css') }}">
    <!-- Icons & Favicons -->
    <link rel="icon" href="{{ asset('assets/favicon.png') }}">
    <link href="{{ asset('assets/images/apple-icon-touch.png') }}" rel="apple-touch-icon" />
    <!--[if IE]>
    <link rel="shortcut icon" href="https://ballysac.com/wp-content/themes/BallysAtlanticCityCasino/favicon.ico">
    <![endif]-->
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage" content="{{ asset('assets/images/win8-tile-icon.png') }}">
    <meta name="theme-color" content="#121212">

    <link rel="pingback" href="xmlrpc.php">

    <title>Bally&#039;s Atlantic City Hotel and Casino</title>

    <!-- The SEO Framework by Sybre Waaijer -->
    <meta name="robots" content="max-snippet:-1,max-image-preview:standard,max-video-preview:-1" />
    <meta name="description" content="Bally’s Atlantic City Hotel and Casino puts you right at the center of the action at the heart of Atlantic City, N.J. Book your stay at Bally&#8217;s AC today." />
    <meta property="og:image" content="wp-{{ asset('uploads/2020/12/BAC_Logo_blk-scaled.jpg') }}" />
    <meta property="og:image:width" content="2560" />
    <meta property="og:image:height" content="898" />
    <meta property="og:image:alt" content="Bally&#039;s Atlantic City Hotel and Casino" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Bally&#039;s Atlantic City Hotel and Casino" />
    <meta property="og:description" content="Bally’s Atlantic City Hotel and Casino puts you right at the center of the action at the heart of Atlantic City, N.J. Book your stay at Bally&#8217;s AC today." />
    <meta property="og:url" content="index.html" />
    <meta property="og:site_name" content="Bally&#039;s Atlantic City Hotel and Casino" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Bally&#039;s Atlantic City Hotel and Casino" />
    <meta name="twitter:description" content="Bally’s Atlantic City Hotel and Casino puts you right at the center of the action at the heart of Atlantic City, N.J. Book your stay at Bally&#8217;s AC today." />
    <meta name="twitter:image" content="wp-{{ asset('uploads/2020/12/BAC_Logo_blk-scaled.jpg') }}" />
    <meta name="twitter:image:width" content="2560" />
    <meta name="twitter:image:height" content="898" />
    <meta name="twitter:image:alt" content="Bally&#039;s Atlantic City Hotel and Casino" />
    <link rel="canonical" href="index.html" />
    <script type="application/ld+json">{"@context":"https://schema.org","@type":"WebSite","url":"https://ballysac.com/","name":"Bally&#039;s Atlantic City Hotel and Casino","potentialAction":{"@type":"SearchAction","target":"https://ballysac.com/search/{search_term_string}/","query-input":"required name=search_term_string"}}</script>
    <script type="application/ld+json">{"@context":"https://schema.org","@type":"Organization","url":"https://ballysac.com/","name":"Bally&#039;s Atlantic City Hotel and Casino","logo":"https://ballysac.com/wp-content/uploads/2020/12/BAC_Logo_blk-scaled.jpg"}</script>

    <link rel='dns-prefetch' href='http://maxcdn.bootstrapcdn.com/' />
    <link rel='dns-prefetch' href='http://s.w.org/' />
    <link rel="alternate" type="application/rss+xml" title="Bally&#039;s Atlantic City Hotel and Casino &raquo; Feed" href="#" />
    <link rel="alternate" type="application/rss+xml" title="Bally&#039;s Atlantic City Hotel and Casino &raquo; Comments Feed" href="#" />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/ballysac.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
        !function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='wp-block-library-css'  href='{{ asset('assets/css/dist/block-library/style.min40df.css?ver=5.6') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='normalize-css-css'  href='{{ asset('assets/vendor/foundation/css/normalize.min40df.css?ver=5.6') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='foundation-css-css'  href='{{ asset('assets/vendor/foundation/css/foundation.min40df.css?ver=5.6') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='site-css-css'  href='{{ asset('assets/css/style40df.css?ver=5.6') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css'  href='{{ asset('assets/font-awesome/4.3.0/css/font-awesome.min40df.css?ver=5.6') }}' type='text/css' media='all' />
    <link rel="https://api.w.org/" href="{{ asset('json/index.html') }}" /><link rel="alternate" type="application/json" href="{{ asset('json/wp/v2/pages/19.json') }}" /><link rel="alternate" type="application/json+oembed" href="{{ asset('json/oembed/1.0/embed94d9.json?url=https%3A%2F%2Fballysac.com%2F') }}" />
    <link rel="alternate" type="text/xml+oembed" href="{{ asset('json/oembed/1.0/embed9583?url=https%3A%2F%2Fballysac.com%2F&amp;format=xml') }}" />
    <style type="text/css" id="wp-custom-css">
        .page-title {
            display: none;
        }

        .column {
            float: left;
            width: 50%;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }		</style>

    <!-- Drop Google Analytics here -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-X06MLB7TLQ"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-X06MLB7TLQ');
    </script>
    <!-- end analytics -->

</head>

<body class="home page-template page-template-front-page page-template-front-page-php page page-id-19">
<div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
        <div id="container">
            <header class="header" role="banner">
                <div id="inner-header" class="row clearfix">
                    <!-- This navs will be applied to the topbar, above all content
                         To see additional nav styles, visit the /parts directory -->
                    <div class="show-for-medium-up contain-to-grid">
                        <nav class="top-bar" data-topbar>
                            <ul class="title-area">
                                <!-- Title Area -->
                                <li class="name">
                                    <a href="/" rel="nofollow"> <!-- Bally&#039;s Atlantic City Hotel and Casino</a> -->
                                        <img src="{{ asset('assets/images/BAC_Logo_blk.png') }}" alt="bally's logo"></a>
                                </li>
                            </ul>
                            <section class="top-bar-section right">
                                <ul id="menu-main-navigation" class="top-bar-menu right"><li class="divider"></li><li id="menu-item-1831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-1831"><a href="#">Casino</a>
                                        <ul class="sub-menu dropdown">
                                            <li id="menu-item-1081" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1081"><a href="#">Promotions</a></li>
                                            <li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a href="#">Slots and Video Poker</a></li>
                                            <li id="menu-item-1762" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1762"><a href="#">Table Games</a></li>
                                            <li id="menu-item-4998" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4998"><a href="#">Players Club</a></li>
                                            <li id="menu-item-2035" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2035"><a href="#">Casino Credit</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li><li id="menu-item-1767" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-1767"><a href="#">Hotel</a>
                                        <ul class="sub-menu dropdown">
                                            <li id="menu-item-4987" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4987"><a target="_blank" rel="noopener" href="https://ballysac.reztrip.com/">Book Your Stay</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li><li id="menu-item-4844" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4844"><a href="#">Sportsbook</a>
                                        <ul class="sub-menu dropdown">
                                            <li id="menu-item-4970" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4970"><a href="#">FanDuel Sportsbook House Rules</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li><li id="menu-item-4845" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-dropdown menu-item-4845"><a href="#">Restaurants</a>
                                        <ul class="sub-menu dropdown">
                                            <li id="menu-item-4846" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4846"><a href="#">Buca Di Beppo</a></li>
                                            <li id="menu-item-4847" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4847"><a href="#">Dunkin’ Donuts</a></li>
                                            <li id="menu-item-4849" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4849"><a href="#">Guy Fieri’s Chophouse</a></li>
                                            <li id="menu-item-4850" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4850"><a href="#">Harry’s Oyster Bar</a></li>
                                            <li id="menu-item-4851" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4851"><a href="#">Johnny Rockets</a></li>
                                            <li id="menu-item-4852" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4852"><a href="#">Noodle Village</a></li>
                                            <li id="menu-item-4854" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4854"><a href="#">Walt’s Primo Pizza</a></li>
                                            <li id="menu-item-4853" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4853"><a href="#">Sack O’ Subs</a></li>
                                            <li id="menu-item-5011" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5011"><a href="#">Sportsbook Lounge</a></li>
                                        </ul>
                                    </li>
                                    <li class="divider"></li><li id="menu-item-4962" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4962"><a href="#">Contact</a></li>
                                    <li class="divider"></li><li id="menu-item-4961" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4961"><a href="#">Careers</a></li>
                                    <li class="divider"></li><li id="menu-item-4962" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4961"><a href="/admin">Player Portal</a></li>
                                </ul>		</section>
                        </nav>
                    </div>

                    <div class="sticky fixed show-for-small">
                        <nav class="tab-bar">
                            <section class="middle tab-bar-section">
                                <a href="index.html" rel="nofollow"> <h1 class="title">Bally's Casino & Hotel</h1></a>
                            </section>
                            <section class="left-small">
                                <a href="#" class="left-off-canvas-toggle menu-icon"><span><i class="fa fab fa-bars"></i></span></a>
                            </section>
                        </nav>
                    </div>

                    <aside class="left-off-canvas-menu show-for-small-only">
                        <ul class="off-canvas-list">
                            <!-- <li><label>Navigation</label></li> -->
                            <a href="index.html"><span class="home-link">Home</span></a>

                            <ul id="menu-main-navigation-1" class="off-canvas-list"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1831"><a href="casino/index.html">Casino</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1081"><a href="#">Promotions</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a href="#">Slots and Video Poker</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1762"><a href="#">Table Games</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4998"><a href="#">Players Club</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2035"><a href="#">Casino Credit</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1767"><a href="#">Hotel</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4987"><a target="_blank" rel="noopener" href="https://ballysac.reztrip.com/">Book Your Stay</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4844"><a href="#">Sportsbook</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4970"><a href="#">FanDuel Sportsbook House Rules</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4845"><a href="#">Restaurants</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4846"><a href="#">Buca Di Beppo</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4847"><a href="#">Dunkin’ Donuts</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4849"><a href="#">Guy Fieri’s Chophouse</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4850"><a href="#">Harry’s Oyster Bar</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4851"><a href="#">Johnny Rockets</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4852"><a href="#">Noodle Village</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4854"><a href="#">Walt’s Primo Pizza</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4853"><a href="#">Sack O’ Subs</a></li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5011"><a href="#">Sportsbook Lounge</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4962"><a href="#">Contact</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4961"><a href="#">Careers</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4961"><a href="/admin">Player Portal</a></li>
                            </ul>	</ul>
                    </aside>

                    <a class="exit-off-canvas"></a>					  </div>  <!-- end #inner-header -->

            </header> <!-- end .header -->

            <div id="content">

                <div id="inner-content" class="row clearfix">

                    <div class="large-12 medium-12 columns">
                        <div class="row clearfix fullwidth slider">
                            <div aria-live="polite" id="soliloquy-container-20" class="soliloquy-container soliloquy-transition-fade  soliloquy-theme-base" style="max-width:1185px;max-height:545px;"><ul id="soliloquy-20" class="soliloquy-slider soliloquy-slides soliloquy-wrap soliloquy-clear"><li aria-hidden="true" class="soliloquy-item soliloquy-item-1 soliloquy-image-slide" draggable="false" style="list-style:none"><a href="https://ballybracketchallenge.com/" class="soliloquy-link" title="Bally_BracketChallenge2021-Assets-Slider" target="_blank"><img id="soliloquy-image-5010" class="soliloquy-image soliloquy-image-1" src="{{ asset('uploads/2021/03/Bally_BracketChallenge2021-Assets-Slider-1185x545_c.jpg') }}" alt="Bally Bracket Challenge" /></a></li><li aria-hidden="true" class="soliloquy-item soliloquy-item-2 soliloquy-image-slide" draggable="false" style="list-style:none"><img id="soliloquy-image-4958" class="soliloquy-image soliloquy-image-2" src="{{ asset('uploads/2021/02/AC_PropertyPhotos-WebsiteSlider-1185x545_c.jpg') }}" alt="Bally&#039;s Atlantic City Entrance" /></li><li aria-hidden="true" class="soliloquy-item soliloquy-item-3 soliloquy-image-slide" draggable="false" style="list-style:none"><a href="#" class="soliloquy-link" title="AC_RewardsCards-Header"><img id="soliloquy-image-5000" class="soliloquy-image soliloquy-image-3" src="{{ asset('uploads/2015/11/AC_RewardsCards-Header-1185x545_c.png') }}" alt="Did you get your new players card? Click to learn more." /></a></li><li aria-hidden="true" class="soliloquy-item soliloquy-item-4 soliloquy-image-slide" draggable="false" style="list-style:none"><a href="#" class="soliloquy-link" title="AC_COVID19-Protocols-WebsiteSlider"><img id="soliloquy-image-4977" class="soliloquy-image soliloquy-image-4" src="{{ asset('uploads/2021/02/AC_COVID19-Protocols-WebsiteSlider-1-1185x545_c.jpg') }}" alt="Our COVID-19 Response" /></a></li><li aria-hidden="true" class="soliloquy-item soliloquy-item-5 soliloquy-image-slide" draggable="false" style="list-style:none"><img id="soliloquy-image-4927" class="soliloquy-image soliloquy-image-5" src="{{ asset('uploads/2021/01/ballys-tower-penthouse-1-bed-suite-000-0667-1280x1280-1.jpg') }}" alt="ballys-tower-penthouse-1-bed-suite-000-0667-1280&#215;1280" /></li><li aria-hidden="true" class="soliloquy-item soliloquy-item-6 soliloquy-image-slide" draggable="false" style="list-style:none"><img id="soliloquy-image-4928" class="soliloquy-image soliloquy-image-6" src="{{ asset('uploads/2021/01/ballysexteriorwboat-06-4c-1280x1280-1.jpg') }}" alt="ballysexteriorwboat-06-4c-1280&#215;1280" /></li><li aria-hidden="true" class="soliloquy-item soliloquy-item-7 soliloquy-image-slide" draggable="false" style="list-style:none"><a href="#" class="soliloquy-link" title="AC_PropertyPhotos-Restaurants-Slider"><img id="soliloquy-image-5002" class="soliloquy-image soliloquy-image-7" src="{{ asset('uploads/2021/03/AC_PropertyPhotos-Restaurants-Slider-1185x545_c.jpg') }}" alt="Click to View Restaurant Options" /></a></li></ul></div><noscript><style type="text/css">#soliloquy-container-20{opacity:1}</style></noscript>						</div>
                    </div>
                    <!--					<div class="large-12 medium-12 columns frontpage-content-slider">
                                            <div class="row clearfix">
                                                <h2>Upcoming Events</h2>
                                                <div class="bx-outer-wrapper">
                                                    <ul class="bxslider">
                                                                                    </ul>
                                                </div>


                                            </div>
                                        </div>	-->
                    <div id="main" class="large-12 medium-12 columns frontpage-content" role="main">
                        <h1>Bally&#8217;s Atlantic City Hotel and Casino</h1>
                        <p>At Bally’s AC, enjoy fast paced Atlantic City gaming action. Play the most exciting games on the Boardwalk at Bally’s, including games like Baccarat, Blackjack, Roulette, Spanish 21, Let it Ride, Pai Gow, Pai Gow Tiles and Craps, to name a few. For the Poker enthusiast, Bally’s also offers Specialty Poker Games like Mississippi Stud, 3-Card Poker, 4-Card Poker, Heads Up Hold’em and High Card Flush. For a more relaxed experience, sit back and play slots such as progressives, video reels and video poker. For the sports fan in all of us, stop by our newest addition &#8211; FanDuel Sportsbook.</p>
                    </div> <!-- end #main -->

                    <div class="large-12 medium-12 columns frontpage-content-links">
                        <div class="row clearfix">
                            <h2>Quick Links</h2>
                            <a href="#">
                                <div class="large-3 medium-3 small-12 columns quick-links">
                                    <img src="{{ asset('uploads/2020/12/1565807052255.jpg') }}" alt="" />
                                    <h3 class="link-title">TABLE GAMES</h3>
                                    <div class="mask">
                                        TABLE GAMES									</div>
                                </div>
                            </a>
                            <a href="#">
                                <div class="large-3 medium-3 small-12 columns quick-links">
                                    <img src="{{ asset('uploads/2020/12/1608754488022.png') }}" alt="" />
                                    <h3 class="link-title">SLOTS</h3>
                                    <div class="mask">
                                        SLOTS									</div>
                                </div>
                            </a>
                            <a href="#">
                                <div class="large-3 medium-3 small-12 columns quick-links">
                                    <img src="{{ asset('uploads/2021/01/1608598460750.jpg') }}" alt="" />
                                    <h3 class="link-title">DINING</h3>
                                    <div class="mask">
                                        DINING									</div>
                                </div>
                            </a>
                            <a href="#">
                                <div class="large-3 medium-3 small-12 columns quick-links">
                                    <img src="{{ asset('uploads/2021/02/AC_LinkHotel.jpg') }}" alt="" />
                                    <h3 class="link-title">HOTEL</h3>
                                    <div class="mask">
                                        HOTEL									</div>
                                </div>
                            </a>
                        </div>
                    </div>

                </div> <!-- end #inner-content -->

            </div> <!-- end #content -->

            <footer class="footer" role="contentinfo">
                <div id="inner-footer" class="row clearfix fullwidth">
                    <div class="large-12 medium-12 columns">
                        <div id="execphp-2" class="widget-odd widget-first widget-1 widget widget_execphp">			<div class="execphpwidget"><p>An inherent risk of exposure to COVID-19 exists in any public place where people are present. COVID-19 is an extremely contagious disease that can lead to severe illness and even death. According to the <a href="https://www.cdc.gov/coronavirus/2019-nCoV/index.html">Centers for Disease Control and Prevention,</a> senior citizens and guests with underlying medical conditions are especially vulnerable. By visiting Bally's Atlantic City and Casino you voluntarily assume all risks related to exposure to COVID-19.</p>

                                <a href="https://twitter.com/BallysAC"><span class="fa fa-twitter-square"title="Twitter" target="_blank"></span><span class="screen-reader-text">Like Us on Facebook</span></a>

                                <a href="https://www.facebook.com/BallysAtlanticCity"><span class="fa fa-facebook-square"title="Twitter" target="_blank"></span><span class="screen-reader-text">Follow Us on Twitter</span></a>

                                <a href="https://www.instagram.com/ballysac/"><span class="fa fa-instagram"title="instagram" target="_blank"></span><span class="screen-reader-text">Follow Us on Instagram</span></a>


                                <style>a span.screen-reader-text {
                                        clip: rect(1px, 1px, 1px, 1px);
                                        position: absolute !important;
                                        height: 1px;
                                        width: 1px;
                                        overflow: hidden;
                                        color:#fff;
                                        font-size:20px;
                                    }</style>
                            </div>
                        </div><div id="nav_menu-2" class="widget-even widget-2 favorite-links-footer-widget widget widget_nav_menu"><h2 class="widgettitle">FAVORITE LINKS</h2><div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-150" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-150"><a href="#">Promotions</a></li>
                                    <li id="menu-item-1783" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1783"><a href="#">Hotel</a></li>
                                    <li id="menu-item-1784" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1784"><a href="#">Table Games</a></li>
                                    <li id="menu-item-174" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-174"><a href="#">Slots and Video Poker</a></li>
                                </ul></div></div><div id="execphp-3" class="widget-odd widget-last widget-3 join-our-mailing-list-widget widget widget_execphp"><h2 class="widgettitle">BALLY&#8217;S CORP. PROPERTIES</h2>			<div class="execphpwidget"><a href="https://www.twinriver.com/" target="_blank" rel="noopener"><img src="{{ asset('uploads/2021/01/Bally_Individual-AllProperties-01.png') }}" width="30%" alt="Twin River Casino Hotel"> </a>
                                <a href="https://www.hrhcbiloxi.com/" target="_blank" rel="noopener"><img src="{{ asset('uploads/2021/01/Bally_Individual-AllProperties-02.png') }}" width="30%" alt="Hard Rock Hotel & Casino Biloxi"> </a>
                                <a href="https://www.goldenmardigras.com/" target="_blank" rel="noopener"><img src="{{ asset('uploads/2021/01/Bally_Individual-AllProperties-03.png') }}" width="30%" alt="Mardi Gras Casinos"> </a>
                                <br>
                                <a href="https://www.eldoradoshreveport.com/" target="_blank" rel="noopener"><img src="{{ asset('uploads/Bally_Individual-AllProperties-10.png') }}" width="30%" alt="ElDorado Shreveport"> </a>
                                <a href="https://www.twinrivertiverton.com/" target="_blank" rel="noopener"><img src="{{ asset('uploads/2021/01/Bally_Individual-AllProperties-05.png ') }}" width="30%" alt="Tiverton Casino Hotel"> </a>
                                <a href="https://www.doverdowns.com/" target="_blank" rel="noopener"><img src="{{ asset('uploads/2021/01/Bally_Individual-AllProperties-06.png') }}" width="30%" alt="Dover Downs Hotel & Casino"> </a>
                                <br>
                                <a href="http://www.mihiracing.com/mhre/" target="_blank" rel="noopener"><img src="{{ asset('uploads/2021/01/Bally_Individual-AllProperties-07.png') }}" width="30%" alt="Arapahoe Park"> </a>
                                <a href="https://casinokc.com/" target="_blank" rel="noopener"><img src="{{ asset('uploads//2021/01/Bally_Individual-AllProperties-08.png') }}" width="30%" alt="Casino KC"> </a>
                                <a href="https://casinovb.com/" target="_blank" rel="noopener"><img src="{{ asset('uploads/2021/01/Bally_Individual-AllProperties-09.png') }}" width="30%" alt="Casino Vicksburg"> </a>
                                <br></div>
                        </div>								<div style="clear: both"></div>
                        <div id="execphp-13" class="widget-odd widget-last widget-first widget-1 post-footer widget_execphp center">			<div class="execphpwidget"><p>1900 Pacific Ave.<br/>
                                    Atlantic City, NJ 08401<br/>
                                    Tel: (609) 340-2000 </p>

                                <p>Copyright ©2021 Bally's Atlantic City Hotel &amp; Casino®. All rights reserved. | <a href="#">Privacy Policy</a> | <a href="#">Cookie Policy</a></p></div>
                        </div>
                    </div>
                </div> <!-- end #inner-footer -->
            </footer> <!-- end .footer -->
        </div> <!-- end #container -->
    </div> <!-- end .inner-wrap -->
</div> <!-- end .off-canvas-wrap -->
<link rel='stylesheet' id='soliloquy-lite-style-css'  href='{{ asset('plugins/soliloquy-lite/assets/css/soliloquyc141.css?ver=2.6.1') }}' type='text/css' media='all' />
<script type='text/javascript' src='{{ asset('assets/vendor/foundation/js/vendor/jquery4c71.js?ver=2.1.3') }}' id='jquery-js'></script>
<script type='text/javascript' src='{{ asset('assets/vendor/foundation/js/vendor/modernizrf7ff.js?ver=2.8.3') }}'></script>
<script type='text/javascript' src='{{ asset('assets/vendor/foundation/js/foundation.min40df.js?ver=5.6') }}' id='foundation-js-js'></script>
<script type='text/javascript' src='{{ asset('assets/js/scripts40df.js?ver=5.6') }}' id='site-js-js'></script>
<script type='text/javascript' src='{{ asset('assets/js/jquery.bxslider40df.js?ver=5.6') }}' id='bx-slider-js'></script>
<script type='text/javascript' src='{{ asset('assets/js/isotope.pkgd.min40df.js?ver=5.6') }}' id='isotope-js'></script>
<script type='text/javascript' src='{{ asset('assets/js/wp-embed.min40df.js?ver=5.6') }}' id='wp-embed-js'></script>
<script type='text/javascript' src='{{ asset('plugins/soliloquy-lite/assets/js/min/soliloquy-minc141.js?ver=2.6.1') }}' id='soliloquy-lite-script-js'></script>
<script type="text/javascript">
    if ( typeof soliloquy_slider === 'undefined' || false === soliloquy_slider ) {soliloquy_slider = {};}jQuery('#soliloquy-container-20').css('height', Math.round(jQuery('#soliloquy-container-20').width()/(1185/545)));jQuery(window).load(function(){var $ = jQuery;var soliloquy_container_20 = $('#soliloquy-container-20'),soliloquy_20 = $('#soliloquy-20');soliloquy_slider['20'] = soliloquy_20.soliloquy({slideSelector: '.soliloquy-item',speed: 650,pause: 6000,auto: 1,useCSS: 0,keyboard: true,adaptiveHeight: 1,adaptiveHeightSpeed: 400,infiniteLoop: 1,mode: 'fade',pager: 1,controls: 1,nextText: '',prevText: '',startText: '',stopText: '',onSliderLoad: function(currentIndex){soliloquy_container_20.find('.soliloquy-active-slide').removeClass('soliloquy-active-slide').attr('aria-hidden','true');soliloquy_container_20.css({'height':'auto','background-image':'none'});if ( soliloquy_container_20.find('.soliloquy-slider li').length > 1 ) {soliloquy_container_20.find('.soliloquy-controls').fadeTo(300, 1);}soliloquy_20.find('.soliloquy-item:not(.soliloquy-clone):eq(' + currentIndex + ')').addClass('soliloquy-active-slide').attr('aria-hidden','false');soliloquy_container_20.find('.soliloquy-clone').find('*').removeAttr('id');soliloquy_container_20.find('.soliloquy-controls-direction').attr('aria-label','carousel buttons').attr('aria-controls', 'soliloquy-container-20');soliloquy_container_20.find('.soliloquy-controls-direction a.soliloquy-prev').attr('aria-label','previous');soliloquy_container_20.find('.soliloquy-controls-direction a.soliloquy-next').attr('aria-label','next');},onSlideBefore: function(element, oldIndex, newIndex){soliloquy_container_20.find('.soliloquy-active-slide').removeClass('soliloquy-active-slide').attr('aria-hidden','true');$(element).addClass('soliloquy-active-slide').attr('aria-hidden','false');},onSlideAfter: function(element, oldIndex, newIndex){},});});			</script>
</body>
</html> <!-- end page -->
