<?php

use App\Http\Controllers\ImportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('/register', 'Auth\RegisterController@register');
});

Route::get('/', function () {
    return redirect()->route('voyager.login');
});
Route::post('/register', 'Auth\RegisterController@storeUser')->name('register');
Route::post('/view-flipbook-by-account-id-april', 'Voyager\VoyagerController@viewFlipbookByAccountIDApril')->name('view-flipbook-by-account-id-april');
Route::post('/view-flipbook-by-account-id-may', 'Voyager\VoyagerController@viewFlipbookByAccountIDMay')->name('view-flipbook-by-account-id-may');
Route::post('/view-player-dashboard-by-account-id', 'Voyager\VoyagerController@viewPlayerDashBoardByAccountId')->name('view-player-dashboard-by-account-id');
Route::get('/view-player-dashboard-by-account-id/{account_id}', 'Voyager\VoyagerController@getViewPlayerDashBoardByAccountId');

Route::get('forget-password', 'Auth\ForgotPasswordController@getEmail');
Route::post('forget-password', 'Auth\ForgotPasswordController@postEmail');

Route::get('reset-password/{token}', 'Auth\ResetPasswordController@getPassword');
Route::post('reset-password', 'Auth\ResetPasswordController@updatePassword');

